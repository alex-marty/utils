from typing import Dict, List

from fastapi import Body, FastAPI
import numpy as np
import pandas as pd
from pydantic import BaseModel


class DummyEstimator:
    feature_names = ["a", "b"]
    pred_names = ["x", "y", "z"]

    def predict(self, X):
        return np.stack(
            [np.mean(X, axis=1), np.min(X, axis=1), np.max(X, axis=1)], axis=1
        )

    @classmethod
    def clean_named_input(cls, x: List[Dict[str, float]]) -> np.array:
        x = pd.DataFrame(x)
        if set(x.columns) != set(cls.feature_names):
            raise ValueError(f"invalid feature set: {x.columns}")
        return x[cls.feature_names].values

    @classmethod
    def make_named_output(cls, preds: np.array) -> List[Dict[str, float]]:
        if preds.ndim != 2:
            raise ValueError(f"invalid predictions dimension: {preds.ndim}")
        if preds.shape[1] != len(cls.pred_names):
            raise ValueError(f"invalid predictions shape: {preds.shape}")
        return [{n: v for n, v in zip(model.pred_names, row)} for row in preds]


model = DummyEstimator()

app = FastAPI(title="Simple ML API 🤩")


@app.get("/api/v1/status")
def status():
    return {"status": "OK"}


@app.post("/api/v1/predict_raw", response_model=List[List[float]])
def predict(input: List[List[float]] = Body(...)):
    x = np.array(input)
    preds = model.predict(x)
    return preds.tolist()


@app.post("/api/v1/predict", response_model=List[Dict[str, float]])
def predict(input: List[List[float]] = Body(...)):
    x = np.array(input)
    preds = model.predict(x)
    return model.make_named_output(preds)


@app.post("/api/v1/predict_named", response_model=List[Dict[str, float]])
def predict(input: List[Dict[str, float]] = Body(...)):
    x = pd.DataFrame(input)
    if set(x.columns) != set(model.feature_names):
        raise ValueError(f"invalid feature set: {x.columns}")
    x = x[model.feature_names]
    preds = model.predict(x.values)
    return model.make_named_output(preds)


