# Minimalist ML Model Serving App with FastAPI

Requirements:

```shell
pip install fastapi numpy pandas pydantic uvicorn
```

Run with:

```shell
uvicorn ml_app:app --reload
```
