# Requires beautifulsoup, lxml, click
import json

from bs4 import BeautifulSoup
import click


@click.command()
@click.argument("input_html", type=click.File("r"))
@click.argument("output_ipynb", type=click.File("w"))
def main(input_html, output_ipynb):
    text = input_html.read()
    soup = BeautifulSoup(text, "lxml")
    # see some of the html
    print(soup.div)
    dictionary = {"nbformat": 4, "nbformat_minor": 1, "cells": [], "metadata": {}}
    for d in soup.findAll("div"):
        if "class" in d.attrs.keys():
            for clas in d.attrs["class"]:
                if clas in ["text_cell_render", "input_area"]:
                    # code cell
                    if clas == "input_area":
                        cell = {}
                        cell["metadata"] = {}
                        cell["outputs"] = []
                        cell["source"] = [d.get_text()]
                        cell["execution_count"] = None
                        cell["cell_type"] = "code"
                        dictionary["cells"].append(cell)

                    else:
                        cell = {}
                        cell["metadata"] = {}

                        cell["source"] = [d.decode_contents()]
                        cell["cell_type"] = "markdown"
                        dictionary["cells"].append(cell)
    output_ipynb.write(json.dumps(dictionary))


if __name__ == "__main__":
    main()
