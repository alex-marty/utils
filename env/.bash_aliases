# Set tabname, for use with iTerm2, including tmux integration on remote server
function tabname {
  if [ -z $TMUX ] ; then
    printf "\e]1;$@\a"
  else
    tmux rename-window $@
  fi
}
