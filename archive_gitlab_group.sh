#!/usr/bin/env bash
# Download archives of all Gitlab repositories in a specified group recursively

# Generate private token at https://gitlab.com/-/profile/personal_access_tokens, minimum required scopes are read_api and read_repository
PRIVATE_TOKEN="glpat-xxxxxxxxxxxxxxxxxxxx"
GROUP_ID="12345678"
GITLAB_HOST="gitlab.com"
ARCHIVE_FORMAT=".zip"

IFS=$'\n' # Set field separator to newline for for-loop, instead of all space-like chars

# Warning: beware of pagination, here we retrieve one page of max 100 projects
for project in $(curl -s --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://$GITLAB_HOST/api/v4/groups/$GROUP_ID/projects?include_subgroups=true&per_page=100" | jq -rc ".[]");
do
    ns_path=$(echo $project | jq -r ".namespace.full_path");
    proj_path=$(echo $project | jq -r ".path");
    proj_id=$(echo $project | jq -r ".id");
    echo $proj_id, $proj_path, $ns_path;
    mkdir -p "$ns_path";
    curl -s --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://$GITLAB_HOST/api/v4/projects/$proj_id/repository/archive$ARCHIVE_FORMAT" > "$ns_path/$proj_path$ARCHIVE_FORMAT";
done
